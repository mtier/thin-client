# M:Tier Thin Client

![Screenshot](public/thinclient_scrot.png)

The m:tier thin client is a small application written in Python that
can be executed as a window manager replacement. It provides a clean
UI for the thin client hardware in order to allow the user to easily
execute all the needed applications.

## FAQ

- _What needs to be installed to be able to run this Python application?_
  Python 3 (tested with 3.2/3.3) and PyGObject.
- _What packages need to be installed on Ubuntu/Debian?_
  python3.4-dev gir1.2-gtksource-3.0 python3-gi python3-gi-cairo
- _Why do I get `ImportError: No module named ifstat`?_
  The thin client requires the ifstat Python module which has to be
  compiled by issuing the `PYTHON=python-${VERSION} make` command in
  the top-level directory of the thin client code.

## Supported Operating Systems
It was developed on OpenBSD, but can run on any *NIX-like operating
system on which Python is available.

## Available applications
The following applications are configured by default in the Thin
Client configuration file.  Using these applications one can log into
any remote system.  Wether it's using OpenSSH, NX, VNC, RDP, it's
there.

- Google Chrome
- OpenNX
- Remmina

## Extending
Extending the thinclient software is really easy and can be done by
editing the configuration file itself.  You can add as many
applications to it as you want. For further configuration options one
has to edit the Thin client Python script itself, which requires some
minor Python programming skills.
