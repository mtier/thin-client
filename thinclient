#!/usr/bin/env python

"""
  Copyright (c) 2010-2022 M:Tier Ltd.

  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
"""

import os
import pwd
import signal
import subprocess
import sys
from ifstat import ifstat

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('PangoCairo', '1.0')

from gi.repository import GLib
from gi.repository import Gtk
from gi.repository import GObject
from gi.repository import Gdk
from gi.repository.GdkPixbuf import Pixbuf
from gi.repository import Pango
from gi.repository import PangoCairo
import cairo

import math
from datetime import datetime

########## CONFIGURATION ##########
CMD_REBOOT = 'sudo reboot'
CMD_SHUTDOWN = 'sudo halt -p'
CMD_XRANDR = 'xrandr --auto'
CMD_TERMINAL = 'xterm'

ICON_SIZE = 96
NB_COL = 6
SPACING = 20
SHOW_LABEL = True
INTERFACE = "em0"
MARGIN_TOP = 20
CLOCK_H = 64
CLOCK_W = 64
DATE_FORMAT = "%Y-%m-%d"
DATE_FONT = "sans serif 13"
########## CONFIGURATION END ##########

class Date(Gtk.DrawingArea):
    def __init__(self):
        super(Date, self).__init__()
        self.connect('draw', self.expose)
        self.timer = GLib.timeout_add(1000, self.update_date)

    def expose(self, widget, event):
        context = widget.get_property('window').cairo_create()

        time = datetime.now()

        self.layout = self.create_pango_layout(time.strftime(DATE_FORMAT))
        context.set_source_rgba(0.8, 0.8, 0.8)
        self.layout.set_font_description(Pango.FontDescription(DATE_FONT))
        fontw, fonth = self.layout.get_pixel_size()
        alloc = self.get_allocation()
        context.move_to(0, (alloc.height - fonth) / 2)
        PangoCairo.update_layout(context, self.layout)
        PangoCairo.show_layout(context, self.layout)

        return False

    def update_date(self):
        alloc = self.get_allocation()
        rect = Gdk.Rectangle()
        rect.x, rect.y, rect.width, rect.height = (0, 0, alloc.width, alloc.height)
        self.get_property('window').invalidate_rect(rect, False)

        return True

class Clock(Gtk.DrawingArea):
    def __init__(self):
        super(Clock, self).__init__()
        self.connect('draw', self.expose)
        self.radius = 0.42
        self.line_width = 0.05
        self.timer = GLib.timeout_add(1000, self.update_time)

    def expose(self, widget, event):
        context = widget.get_property('window').cairo_create()
        x, y, w, h = context.clip_extents()
        context.rectangle(x, y, w, h)
        context.clip()

        self.draw(context)

        return False

    def draw(self, context):
        width = CLOCK_W
        height = CLOCK_H

        context.scale(width, height)
        context.translate(0.5, 0.5)
        context.set_line_width(self.line_width)

        context.save()
        context.set_source_rgba(1.0, 1.0, 1.0, 0.0)
        context.paint()
        context.restore()
        context.arc(0, 0, self.radius, 0, 2 * math.pi)
        context.save()
        context.set_source_rgba(1.0, 1.0, 1.0, 0.8)
        context.fill_preserve()
        context.restore()
        context.stroke_preserve()
        context.clip()

        # clock ticks
        for i in range(12):
            inset = 0.05

            context.save()
            context.set_line_cap(cairo.LINE_CAP_ROUND)

            if i % 3 != 0:
                inset = inset * 0.8
                context.set_line_width(0.03)

            context.move_to((self.radius - inset) * math.cos(i * math.pi / 6),
                            (self.radius - inset) * math.sin(i * math.pi / 6))
            context.line_to(self.radius * math.cos(i * math.pi / 6),
                            self.radius * math.sin(i * math.pi / 6))
            context.stroke()
            context.restore()

        current_time = datetime.now()
        hours = current_time.hour * math.pi / 6
        minutes = current_time.minute * math.pi / 30
        seconds = current_time.second * math.pi / 30

        context.save()
        context.set_line_cap(cairo.LINE_CAP_ROUND)

        # draw the seconds hand
        context.save()
        context.set_line_width(self.line_width / 3)
        context.set_source_rgba(0.7, 0.7, 0.7, 0.8) # gray
        context.move_to(0, 0)
        context.line_to(math.sin(seconds) * (self.radius * 0.9),
                        -math.cos(seconds) * (self.radius * 0.9))
        context.stroke()
        context.restore()

        # draw the minutes hand
        context.set_source_rgba(0.117, 0.337, 0.612, 0.9) # blue
        context.move_to(0, 0)
        context.line_to(math.sin(minutes + seconds / 60) * (self.radius * 0.8),
                        -math.cos(minutes + seconds / 60) * (self.radius * 0.8))
        context.stroke()

        # draw the hours hand
        context.set_source_rgba(0.337, 0.612, 0.117, 0.9) # green
        context.move_to(0, 0)
        context.line_to(math.sin(hours + minutes / 12.0) * (self.radius * 0.5),
                        -math.cos(hours + minutes / 12.0) * (self.radius * 0.5))
        context.stroke()
        context.restore()

        # draw a little dot in the middle
        context.arc(0, 0, self.line_width / 3.0, 0, 2 * math.pi)
        context.fill()

    def update_time(self):
        alloc = self.get_allocation()
        rect = Gdk.Rectangle()
        rect.x, rect.y, rect.width, rect.height = (0, 0, alloc.width, alloc.height)
        self.get_property('window').invalidate_rect(rect, False)

        return True

class ThinClient():
    def __init__(self):
        self.config = read_config()
        display = Gdk.Display.get_default()
        monitor = display.get_monitor(0)
        geometry = monitor.get_geometry()
        self.screen_w, self.screen_h = geometry.width, geometry.height

        # UI window config
        self.window = Gtk.Window()
        self.window.set_title("M:Tier Thin Client")
        self.window.set_name("mtc")
        self.window.connect("destroy", self.doquit)
        self.window.connect("key-press-event", self.onkeypress)
        self.window.set_border_width(10)

        self.window.set_size_request(self.screen_w, self.screen_h)
        self.window.move(0, 0)

        self.window.set_decorated(False)
        self.window.set_keep_below(True)
        self.window.set_keep_above(False)
        self.window.set_skip_pager_hint(True)
        self.window.stick()

        self.ipackets = 0
        self.opackets = 0

        # main box
        box = Gtk.VBox(homogeneous=False, spacing=50)
        box.set_border_width(0)

        # I use this label to set spacing between top/button
        if MARGIN_TOP:
            self.label = Gtk.Label()
            self.label.set_size_request(-1, MARGIN_TOP)
            box.pack_start(self.label, 0, 0, 0)

        # attach button to table
        table = Gtk.Table(homogeneous=False)

        if SHOW_LABEL:
            table.set_row_spacings(SPACING/2)
        else:
            table.set_row_spacings(SPACING*2)
        table.set_col_spacings(SPACING)

        col = 0
        row = 0

        # M:Tier Logo
        image = Gtk.Image()
        image.set_from_file(basedir + "/img/mtier.png")
        box.pack_start(image, 0, 0, 0)

        for (cmd, icon, text) in self.config:
            but = self.make_custom_but(icon, size=ICON_SIZE, border=25)

            but.connect("key-press-event", self.presskey_button, cmd)
            but.connect("button-press-event", self.click_button, cmd)

            table.attach(but, col, col+1, row, row+1)

            if SHOW_LABEL:
                label = Gtk.Label(label=text)
                table.attach(label, col, col+1, row+1, row+2)
                label.set_size_request(ICON_SIZE+30,-1)

            col += 1
            if col == NB_COL:
                col = 0
                if SHOW_LABEL:
                    row += 2
                else:
                    row += 1

        align = Gtk.Alignment.new(0.5, 0.5, 0, 0)
        align.add(table)
        box.pack_start(align, 0, 0, 0)

        # box extra buttons
        hbox = Gtk.HBox(homogeneous=False, spacing=SPACING)

        but = self.make_custom_but(basedir + "/img/system-shutdown.png", size=48, border=20)
        but.connect("button-press-event", self.click_button, CMD_SHUTDOWN)
        hbox.pack_end(but, 0, 0, 0)

        but = self.make_custom_but(basedir + "/img/system-reboot.png", size=48, border=20)
        but.connect("button-press-event", self.click_button, CMD_REBOOT)
        hbox.pack_end(but, 0, 0, 0)

        but = self.make_custom_but(basedir + "/img/display.png", size=48, border=20)
        but.connect("button-press-event", self.click_button, CMD_XRANDR)
        hbox.pack_end(but, 0, 0, 0)

        self.vimage = Gtk.Image()
        self.vimage.set_from_file(basedir + "/img/VPN-off.png")
        hbox.pack_start(self.vimage, 0, 0, 0)

        self.nimage = Gtk.Image()
        self.nimage.set_from_file(basedir + "/img/data-off.png")
        hbox.pack_start(self.nimage, 0, 0, 0)

        clock = Clock()
        clock.set_size_request(CLOCK_W, CLOCK_H)
        hbox.pack_start(clock, 0, 0, 0)

        date = Date()
        date.set_size_request(128, 64)
        hbox.pack_start(date, 0, 0, 0)

        box.pack_end(hbox, 0, 0, 0)

        # finally add box to main window
        self.window.add(box)

        GLib.timeout_add(100, self.update_net_status_icon)
        GLib.timeout_add(5000, self.update_status_icons)

    def update_status_icons(self):
        process = subprocess.Popen(["/sbin/ipsecctl", "-s", "flow"], stdout=subprocess.PIPE)
        process.wait()
        out, err = process.communicate()
        if process.returncode == 0 and len(out) > 0:
          self.vimage.set_from_file(basedir + "/img/VPN-on.png")
        else:
          self.vimage.set_from_file(basedir + "/img/VPN-off.png")
        return True

    def update_net_status_icon(self):
        stat = ifstat(INTERFACE)

        nopackets = int(stat['opackets'])
        nipackets = int(stat['ipackets'])

        if ((nopackets > self.opackets) or (nipackets > self.ipackets)) and int(stat['linkstate']) >= 0:
          self.nimage.set_from_file(basedir + "/img/data-on.png")
        else:
          self.nimage.set_from_file(basedir + "/img/data-off.png")

        self.ipackets = nipackets
        self.opackets = nopackets

        return True

    def make_custom_but(self, image, size=128, border=20):
        try:
            pixbuf = Pixbuf.new_from_file_at_size(image, size, size)
        except:
            pixbuf = Pixbuf.new_from_file_at_size(basedir + "/img/default.png", size, size)
        but = Gtk.Button()
        but.set_size_request(size+border, size+border)
        img = Gtk.Image()
        img.set_from_pixbuf(pixbuf)

        but.add(img)
        return but

    def click_button(self, widget, event, data=None):
        exec_cmd(data)

    def presskey_button(self, widget, event, data=None):
        key_val = ('Return', 'KP_Enter', 'space')
        if Gdk.keyval_name(event.keyval) in key_val:
            exec_cmd(data)

    def onkeypress(self, widget=None, event=None, data=None):
        if event.get_state() == (Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.MOD1_MASK):
            if event.keyval == Gdk.KEY_s:
                exec_cmd(CMD_TERMINAL)
            elif event.keyval == Gdk.KEY_x:
                exec_cmd(CMD_XRANDR)
            elif event.keyval == Gdk.KEY_q:
                self.doquit()

    def doquit(self, widget=None, data=None):
        Gtk.main_quit()

    def run(self):
        self.window.show_all()
        Gtk.main()

def exec_cmd(cmd=None):
    if cmd and not cmd=='':
        os.system('%s &' % cmd)

def load_theme():
    style_provider = Gtk.CssProvider()
    css = open(basedir + '/mtc.css', 'rb')
    css_data = css.read()
    css.close()
    style_provider.load_from_data(css_data)
    Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(),
                                             style_provider,
                                             Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

def read_config():
    config = []

    if os.access(basedir + "/config", os.F_OK|os.R_OK):
        conf_path = basedir + "/config"
    else:
        print("EE: unable to read config file!")
        sys.exit()

    f = open(conf_path, 'r')

    for line in f:
        if line == '\n' or line.startswith('#'):
            continue
        try:
            line = line.strip('\n')
            (cmd, icon, desc) = line.split('@@')
            config.append((cmd.strip(), icon.strip(), desc.strip()))

        except Exception as e:
            print(("EE: Unable to read config line : %s") % line)
            traceback.print_exc()

    f.close()
    return config

def signal_handler(signal, frame):
    print("II: Caught Keyboard Interrupt")
    sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    basedir = os.path.dirname(os.path.realpath( __file__ ))
    load_theme()
    app = ThinClient()
    app.run()
