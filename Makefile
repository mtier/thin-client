PYTHON ?= python-3.9

ifstat.so: ifstat.c
	${CC} ${CFLAGS} -c ifstat.c `pkg-config --cflags ${PYTHON}` -fPIC -o ifstat.o
	${CC} ${LDFLAGS} -shared ifstat.o -lc `pkg-config --libs ${PYTHON}` -o ifstat.so

all: ifstat.so

clean:
	rm -rf *.o *.so
