/*
 * Copyright (c) 2010-2022 M:Tier Ltd.
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <Python.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static PyObject *
pyifstat(PyObject * self, PyObject * args)
{
#if defined(__linux__)
	FILE	       *fd;
	char		buf[512], spath[MAXPATHLEN];
#else
	int             fd;

	struct ifreq    ifreq;
	struct if_data  v;
#endif
	char            ipackets[40];
	char            opackets[40];
	char            linkstate[40];
	char           *itf;

	if (!PyArg_ParseTuple(args, "s", &itf))
		return NULL;

#if defined(__linux__)
	sprintf(spath, "/sys/class/net/%s/statistics/rx_bytes", itf);
	if ((fd = fopen(spath, "r"))) {
		while(fgets(buf, sizeof(buf), fd)) {
			if (buf[0] == '\r' || buf[0] == '\n')
				continue;
			sprintf(ipackets, "%d", atoi(buf));
		}
		fclose(fd);
	} else {
		sprintf(ipackets, "0");
	}

	sprintf(spath, "/sys/class/net/%s/statistics/tx_bytes", itf);
	if ((fd = fopen(spath, "r"))) {
		while(fgets(buf, sizeof(buf), fd)) {
			if (buf[0] == '\r' || buf[0] == '\n')
				continue;
			sprintf(opackets, "%d", atoi(buf));
		}
		fclose(fd);
	} else {
		sprintf(opackets, "0");
	}

	sprintf(spath, "/sys/class/net/%s/operstate", itf);
	if ((fd = fopen(spath, "r"))) {
		while(fgets(buf, sizeof(buf), fd)) {
			if (buf[0] == '\r' || buf[0] == '\n')
				continue;
			if (strstr(buf, "up"))
				sprintf(linkstate, "1");
			else
				sprintf(linkstate, "0");
		}
		fclose(fd);
	} else {
		sprintf(linkstate, "0");
	}
#else
	fd = socket(AF_INET, SOCK_DGRAM, 0);

	strlcpy(ifreq.ifr_name, itf, IFNAMSIZ - 1);

	ifreq.ifr_data = (caddr_t) & v;

	if (!ioctl(fd, SIOCGIFDATA, &ifreq)) {
		sprintf(ipackets, "%llu", v.ifi_ipackets);
		sprintf(opackets, "%llu", v.ifi_opackets);
		sprintf(linkstate, "%d", v.ifi_link_state);
	} else {
		sprintf(ipackets, "0");
		sprintf(opackets, "0");
		sprintf(linkstate, "0");
	}

	close(fd);
#endif

	return Py_BuildValue("{s:s,s:s,s:s}",
			     "ipackets", ipackets,
			     "opackets", opackets,
			     "linkstate", linkstate);
}

static struct PyMethodDef ifstat_methods[] = {
	{"ifstat",
		pyifstat,
		METH_VARARGS,
	"Displays the interface stats.\n"},
	{NULL, NULL, 0, NULL}
};

#if PY_MAJOR_VERSION == 2
PyMODINIT_FUNC
initifstat()
{
	Py_InitModule3("ifstat", ifstat_methods,
		       "Display network interface stats.\n");
}
#else /* Python 3.x */
static struct PyModuleDef ifstatmodule = {
	PyModuleDef_HEAD_INIT,
	"ifstat",
	NULL,
	-1,
	ifstat_methods
};

PyMODINIT_FUNC
PyInit_ifstat(void)
{
	return PyModule_Create(&ifstatmodule);
}
#endif
